package org.prcs251a.eventtracker;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;

import java.security.SecureRandom;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class Util {
    public static boolean isOnline(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static class Password {
        private static final int iterations = 25*1000;
        private static final int saltLength = 32;
        private static final int keyLength = 256;

        public static String getSaltedHash(String password) throws Exception {
            byte[] salt = SecureRandom.getInstance("SHA1PRNG").generateSeed(saltLength);
            return Base64.encodeToString(salt, Base64.DEFAULT) + "$" + hash(password, salt);
        }

        public static boolean check(String password, String stored) throws Exception{
            String[] saltAndPass = stored.split("\\$");
            if (saltAndPass.length != 2) {
                throw new IllegalStateException("incorrect format");
            }
            String hashOfInput = hash(password, Base64.decode(saltAndPass[0], Base64.DEFAULT));
            return hashOfInput.equals(saltAndPass[1]);
        }

        private static String hash(String password, byte[] salt) throws Exception {
            if (password == null || password.length() == 0)
                throw new IllegalArgumentException("Password was empty");
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            SecretKey key = keyFactory.generateSecret(new PBEKeySpec(
                    password.toCharArray(), salt, iterations, keyLength)
            );
            return Base64.encodeToString(key.getEncoded(), Base64.DEFAULT);
        }
    }
}
