package org.prcs251a.eventtracker.types;

public class Tuple<A, B, C> {
    A first;
    B middle;
    C last;

    public Tuple() {
        first = null;
        middle = null;
        last = null;
    }

    public Tuple(A first, B middle, C last) {
        this.first = first;
        this.middle = middle;
        this.last = last;
    }

    public A getFirst() {
        return first;
    }

    public void setFirst(A first) {
        this.first = first;
    }

    public B getMiddle() {
        return middle;
    }

    public void setMiddle(B middle) {
        this.middle = middle;
    }

    public C getLast() {
        return last;
    }

    public void setLast(C last) {
        this.last = last;
    }
}
