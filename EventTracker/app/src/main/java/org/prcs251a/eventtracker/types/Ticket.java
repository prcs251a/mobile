package org.prcs251a.eventtracker.types;

import android.graphics.Bitmap;

public class Ticket {
    private String reference;
    private Artist artist;
    private Bitmap icon;
    private float price;
    private Venue venue;
    private String tour;
    private String type;

    public Ticket(Artist artist, Bitmap icon, float price, Venue venue, String tour, String type, int eventId) {
        this.artist = artist;
        this.icon = icon;
        this.price = price;
        this.venue = venue;
        this.tour = tour;
        this.type = type;
        this.reference = artist.getId() + venue.getId() + type;
    }

    public Artist getArtist() {
        return artist;
    }

    public Bitmap getIcon() {
        return icon;
    }

    public float getPrice() {
        return price;
    }

    public Venue getVenue() {
        return venue;
    }

    public String getTour() {
        return tour;
    }

    public String getType() {
        return type;
    }

    @Override
    public int hashCode()
    {
        return reference.hashCode();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        Ticket other = (Ticket) o;
        return reference.equals(other.reference);
    }
}
