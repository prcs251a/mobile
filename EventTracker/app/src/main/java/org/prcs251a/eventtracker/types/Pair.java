package org.prcs251a.eventtracker.types;

public class Pair<A, B> {
    private A first;
    private B second;

    public Pair(A first, B second) {
        super();
        this.first = first;
        this.second = second;
    }

    public A first() {
        return first;
    }

    public void first(A first) {
        this.first = first;
    }

    public B second() {
        return second;
    }

    public void second(B second) {
        this.second = second;
    }
}