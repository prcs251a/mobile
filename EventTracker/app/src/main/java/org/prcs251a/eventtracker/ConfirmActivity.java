package org.prcs251a.eventtracker;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import java.text.NumberFormat;

public class ConfirmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Resources res = getResources();
        Bundle bundle = getIntent().getExtras();
        String text = String.format(res.getString(R.string.confirmString), bundle.get("count") + " ticket" + (bundle.getInt("count") >= 2 ? "s " : " "), NumberFormat.getCurrencyInstance().format(bundle.get("cost")), bundle.get("email"), bundle.get("bookingId"));
        ((TextView) findViewById(R.id.confirmText)).setText(text);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        BasketSingleton.getMap().clear();
    }
}
