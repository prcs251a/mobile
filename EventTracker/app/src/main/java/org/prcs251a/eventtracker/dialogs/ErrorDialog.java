package org.prcs251a.eventtracker.dialogs;

import android.app.AlertDialog;
import android.content.Context;

public class ErrorDialog {

    public ErrorDialog(Context ctx, String message) {
        AlertDialog.Builder dlg = new AlertDialog.Builder(ctx);
        dlg.setTitle("An error occurred");
        dlg.setMessage(message);
        dlg.setNeutralButton("Ok", null);
        dlg.create().show();
    }
}
