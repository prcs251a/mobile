package org.prcs251a.eventtracker;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.prcs251a.eventtracker.types.Ticket;

import java.text.NumberFormat;
import java.util.HashMap;

public class BasketActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Basket");
        setContentView(R.layout.activity_basket);
        final LinearLayout rootLayout = (LinearLayout) findViewById(R.id.basketLayout);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }


        if (BasketSingleton.getMap().size() == 0) {
            ((TextView)findViewById(R.id.basketDescription)).setText("Nothing here but us chickens...");
            return;
        }

        int i = 1;
        for (HashMap.Entry<Ticket, Integer> entry : BasketSingleton.getMap().entrySet()) {
            final Ticket t = entry.getKey();
            final int[] quantity = {entry.getValue()};
            RelativeLayout layout = new RelativeLayout(BasketActivity.this);
            GradientDrawable border = new GradientDrawable();
            border.setColor(0xFFFFFFFF);
            border.setStroke(1, 0xFF000000);

            TableRow titleRow = new TableRow(BasketActivity.this);
            TableRow contentRow = new TableRow(BasketActivity.this);
            TableRow quantityRow = new TableRow(BasketActivity.this);
            layout.setId(View.generateViewId());
            final TextView price = new TextView(BasketActivity.this);
            TextView name = new TextView(BasketActivity.this);
            TextView venueName = new TextView(BasketActivity.this);
            TextView ticketType = new TextView(BasketActivity.this);
            NumberPicker quantityPicker = new NumberPicker(BasketActivity.this);
            TextView quantityPrompt = new TextView(BasketActivity.this);

            price.setId(View.generateViewId());
            name.setId(View.generateViewId());
            venueName.setId(View.generateViewId());
            ticketType.setId(View.generateViewId());
            quantityPicker.setId(View.generateViewId());

            RelativeLayout.LayoutParams nameParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            nameParams.bottomMargin = 50;
            nameParams.topMargin = 10;
            nameParams.leftMargin = 10;
            RelativeLayout.LayoutParams venueParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            venueParams.addRule(RelativeLayout.BELOW, name.getId());
            venueParams.bottomMargin = 20;
            venueParams.leftMargin = 10;
            RelativeLayout.LayoutParams ticketParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            ticketParams.addRule(RelativeLayout.BELOW, venueName.getId());
            ticketParams.bottomMargin = 20;
            ticketParams.leftMargin = 10;
            RelativeLayout.LayoutParams priceParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            priceParams.addRule(RelativeLayout.BELOW, ticketType.getId());
            priceParams.bottomMargin = 20;
            priceParams.leftMargin = 10;
            RelativeLayout.LayoutParams quantityParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            quantityParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            quantityParams.rightMargin = 10;
            quantityParams.bottomMargin = 50;

            quantityPicker.setMinValue(0);
            quantityPicker.setMaxValue(999);
            quantityPicker.setValue(quantity[0]);
            quantityPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(final NumberPicker picker, int oldVal, int newVal) {
                    if (newVal == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(BasketActivity.this);
                        builder.setMessage("Remove the ticket from basket?");
                        builder.setPositiveButton("Yes", new Dialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                rootLayout.removeView(findViewById(((RelativeLayout)picker.getParent()).getId()));
                                BasketSingleton.getMap().remove(t);
                                dialog.cancel();
                            }
                        });

                        builder.setNegativeButton("No", new Dialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder.show();
                    }
                    quantity[0] = newVal;
                    BasketSingleton.getMap().put(t, newVal);
                    price.setText(NumberFormat.getCurrencyInstance().format(t.getPrice() * quantity[0]));
                }
            });
            quantityPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
            quantityPicker.setWrapSelectorWheel(false);
            quantityPrompt.setText(R.string.quantityPrompt);
            quantityPicker.setBackground(border);

            price.setText(NumberFormat.getCurrencyInstance().format(t.getPrice() * quantity[0]));
            venueName.setText(t.getVenue().getName());
            ticketType.setText(t.getType());
            name.setText(t.getArtist().getName());
            name.setTypeface(null, Typeface.BOLD);

            layout.addView(name, nameParams);
            layout.addView(venueName, venueParams);
            layout.addView(ticketType, ticketParams);
            layout.addView(price, priceParams);
            layout.addView(quantityPicker, quantityParams);

            rootLayout.addView(layout);
            i++;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_checkout:
                if (BasketSingleton.getMap().size() != 0) {
                    Intent checkoutIntent = new Intent(this, CheckoutActivity.class);
                    startActivityForResult(checkoutIntent, 1);
                }
                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.basket_menu, menu);
        return true;
    }
}
