package org.prcs251a.eventtracker.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class ExceptionDialog extends Exception {

    public ExceptionDialog() {
        super();
    }

    public ExceptionDialog(String message) {
        super(message);
    }

    public void showAlert(Context ctx) {
        AlertDialog.Builder dlg = new AlertDialog.Builder(ctx);
        dlg.setTitle("An error occurred");
        dlg.setMessage(this.getMessage());
        dlg.setPositiveButton("Ok", null);
        dlg.create().show();
    }

    public void showAlert(Context ctx, String btnBody, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder dlg = new AlertDialog.Builder(ctx);
        dlg.setTitle("An error occurred");
        dlg.setMessage(this.getMessage());
        dlg.setNeutralButton("Ok", null);
        dlg.setPositiveButton(btnBody, listener);
        dlg.create().show();
    }
}
