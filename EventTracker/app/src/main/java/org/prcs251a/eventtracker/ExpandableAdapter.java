package org.prcs251a.eventtracker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import org.prcs251a.eventtracker.types.Artist;
import org.prcs251a.eventtracker.types.Venue;

import java.util.HashMap;
import java.util.List;

public class ExpandableAdapter extends BaseExpandableListAdapter {
    private final Context ctx;
    private final Activity activity;
    private final LayoutInflater layoutInflater;
    private HashMap<String, List<Venue>> tourVenueMap;
    private Artist currentArtist;

    public ExpandableAdapter(Activity activity, HashMap<String, List<Venue>> tourVenueMap) {
        this.ctx = activity.getApplicationContext();
        this.layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.tourVenueMap = tourVenueMap;
        this.activity = activity;
    }

    public void setCurrentArtist(Artist currentArtist) {
        this.currentArtist = currentArtist;
    }

    @Override
    public int getGroupCount() {
        return tourVenueMap.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int size = 0;
        for (List<Venue> venueList : tourVenueMap.values()) {
            size += venueList.size();
        }

        return size;
    }

    @Override
    public String getGroup(int groupPosition) {
        int count = 0;
        for (String key : tourVenueMap.keySet()) {
            if (count == groupPosition) {
                return key;
            } else {
                ++count;
            }
        }

        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return tourVenueMap.get(getGroup(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String tour = getGroup(groupPosition);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.group_item, null);
        }
        TextView item = (TextView) convertView.findViewById(R.id.tour);
        item.setTypeface(null, Typeface.BOLD);
        item.setTextColor(Color.BLACK);
        item.setTextSize((float) 22.0);
        item.setText(tour);
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.child_item, null);
        }

        TextView item = (TextView) convertView.findViewById(R.id.event);
        final Venue currentVenue = tourVenueMap.get(getGroup(groupPosition)).get(childPosition);
        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ticketDetails = new Intent(activity, TicketDetailsActivity.class);
                ticketDetails.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ticketDetails.putExtra("tourName", getGroup(groupPosition));
                ticketDetails.putExtra("venueName", tourVenueMap.get(getGroup(groupPosition)).get(childPosition).getName());
                ticketDetails.putExtra("venueId", currentVenue.getId());
                ticketDetails.putExtra("venueDesc", currentVenue.getDescription());
                ticketDetails.putExtra("venueTier1", currentVenue.getTicketLimits().getFirst());
                ticketDetails.putExtra("venueTier2", currentVenue.getTicketLimits().getMiddle());
                ticketDetails.putExtra("venueTier3", currentVenue.getTicketLimits().getLast());
                ticketDetails.putExtra("artistName", currentArtist.getName());
                ticketDetails.putExtra("artistId", currentArtist.getId());
                ticketDetails.putExtra("artistDesc", currentArtist.getDescription());
                ticketDetails.putExtra("eventId", currentVenue.getEventId());
                activity.startActivity(ticketDetails);
            }
        });

        Typeface font = Typeface.createFromAsset(convertView.getContext().getAssets(), "FontAwesome.otf");
        String venueOutput = "<b>" +
                currentVenue.getName() +
                "</b><br /><small>" +
                currentVenue.getDescription() +
                "<br />" +
                ctx.getString(R.string.fa_male) +
                "&nbsp;&nbsp;" +
                currentVenue.getTicketLimits().getFirst() +
                "<br />" +
                ctx.getString(R.string.fa_user_plus) +
                currentVenue.getTicketLimits().getMiddle() +
                "<br />" +
                ctx.getString(R.string.fa_user) +
                "&nbsp;&nbsp;" +
                currentVenue.getTicketLimits().getLast() +
                "</small><br />";

        item.setTypeface(font);
        item.setText(Html.fromHtml(venueOutput));
        item.setTextColor(Color.BLACK);
        item.setTextSize((float) 20.0);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
