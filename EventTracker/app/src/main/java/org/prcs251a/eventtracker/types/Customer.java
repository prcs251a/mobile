package org.prcs251a.eventtracker.types;

public class Customer {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private int county;
    private String city;
    private String addressLine1;
    private String addressLine2;
    private String postcode;
    private String username;


    public Customer(String firstName, String lastName, String email, String password, int county, String city, String addressLine1, String addressLine2, String postcode, String username) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.county = county;
        this.city = city;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.postcode = postcode;
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public int getCounty() {
        return county;
    }

    public String getCity() {
        return city;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getUsername() {
        return username;
    }
}
