package org.prcs251a.eventtracker;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.GridLayout;
import android.widget.NumberPicker;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.prcs251a.eventtracker.api.Provider;
import org.prcs251a.eventtracker.types.Artist;
import org.prcs251a.eventtracker.types.BasketBarActivity;
import org.prcs251a.eventtracker.types.Ticket;
import org.prcs251a.eventtracker.types.Tuple;
import org.prcs251a.eventtracker.types.Venue;

import java.text.NumberFormat;
import java.util.concurrent.ExecutionException;

public class TicketDetailsActivity extends BasketBarActivity {
    private String ticketType;
    private float ticketPrice;
    private String venueName;
    private NumberPicker picker;
    private Integer quantity;
    private float subtotalResult;
    private float totalResult;
    private float currentPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        totalResult = currentPrice = (float) 0.0;
        quantity = 1;
        final NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
        final Bundle bundle = getIntent().getExtras();
        venueName = bundle.getString("venueName");

        setTitle(venueName);
        setContentView(R.layout.activity_ticket_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TableLayout coreLayout = (TableLayout) findViewById(R.id.ticketList);
        picker = (NumberPicker) findViewById(R.id.totalPicker);
        picker.setMaxValue(999);
        picker.setMinValue(1);
        picker.setWrapSelectorWheel(false);
        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                quantity = newVal;
                subtotalResult = currentPrice * quantity;
                totalResult = (subtotalResult + (currentPrice / 10));
                ((TextView) findViewById(R.id.subTotal)).setText(currencyFormatter.format(subtotalResult));
                ((TextView) findViewById(R.id.total)).setText(currencyFormatter.format(totalResult));
            }
        });

        try {
            JsonObject future = Ion.with(TicketDetailsActivity.this)
                .load(Provider.baseUrl + String.format("seatingtiers"))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            JsonArray results = result.getAsJsonArray("$values");
                            for (int i = 0; i < results.size(); i++) {
                                if (results.get(i).getAsJsonObject().get("TIERID").getAsInt() == 1) {
                                    ((TextView)findViewById(R.id.tier1Price)).setText(String.format("%s*", currencyFormatter.format(results.get(i).getAsJsonObject().get("TICKETPRICE").getAsDouble())));
                                } else if (results.get(i).getAsJsonObject().get("TIERID").getAsInt() == 2) {
                                    ((TextView)findViewById(R.id.tier2Price)).setText(String.format("%s*", currencyFormatter.format(results.get(i).getAsJsonObject().get("TICKETPRICE").getAsDouble())));
                                } else if (results.get(i).getAsJsonObject().get("TIERID").getAsInt() == 3) {
                                    ((TextView)findViewById(R.id.tier3Price)).setText(String.format("%s*", currencyFormatter.format(results.get(i).getAsJsonObject().get("TICKETPRICE").getAsDouble())));
                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }).get();

            Log.d("onCreate: ", Provider.baseUrl + String.format("tickets/queryByEvent?eventid=%s", bundle.getInt("eventId")));
            JsonObject future2 = Ion.with(TicketDetailsActivity.this)
                .load(Provider.baseUrl + String.format("tickets/queryByEvent?eventid=%s", bundle.getInt("eventId")))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            int tier1, tier2, tier3;
                            tier1 = tier2 = tier3 = 0;
                            JsonArray results = result.getAsJsonArray("$values");
                            for (int i = 0; i < results.size(); i++) {
                                int currentId = results.get(i).getAsJsonObject().get("TIERID").getAsInt();
                                if (currentId == 1) {
                                    tier1++;
                                } else if (currentId == 2) {
                                    tier2++;
                                } else if (currentId == 3) {
                                    tier3++;
                                }
                            }
                            if ((bundle.getInt("venueTier1") - tier1) > 1000) {
                                ((TextView)findViewById(R.id.tier1Left)).setText(">1000");
                            } else if (((bundle.getInt("venueTier1") - tier1) < 100)) {
                                ((TextView)findViewById(R.id.tier1Left)).setText("<100");
                                ((TextView)findViewById(R.id.tier1Left)).setTextColor(Color.YELLOW);
                            } else if ((bundle.getInt("venueTier1") - tier1) < 50) {
                                ((TextView)findViewById(R.id.tier1Left)).setText(String.valueOf(bundle.getInt("venueTier1") - tier1));
                                ((TextView)findViewById(R.id.tier1Left)).setTextColor(Color.RED);
                            }
                            
                            if ((bundle.getInt("venueTier2") - tier2) > 1000) {
                                ((TextView)findViewById(R.id.tier2Left)).setText(">1000");
                            } else if (((bundle.getInt("venueTier2") - tier2) < 100)) {
                                ((TextView)findViewById(R.id.tier2Left)).setText("<100");
                                ((TextView)findViewById(R.id.tier2Left)).setTextColor(Color.YELLOW);
                            } else if ((bundle.getInt("venueTier2") - tier2) < 50) {
                                ((TextView)findViewById(R.id.tier2Left)).setText(String.valueOf(bundle.getInt("venueTier2") - tier2));
                                ((TextView)findViewById(R.id.tier2Left)).setTextColor(Color.RED);
                            }
                            
                            if ((bundle.getInt("venueTier3") - tier3) > 1000) {
                                ((TextView)findViewById(R.id.tier3Left)).setText(">1000");
                            } else if (((bundle.getInt("venueTier3") - tier3) < 100)) {
                                ((TextView)findViewById(R.id.tier3Left)).setText("<100");
                                ((TextView)findViewById(R.id.tier3Left)).setTextColor(Color.YELLOW);
                            } else if ((bundle.getInt("venueTier3") - tier3) < 50) {
                                ((TextView)findViewById(R.id.tier3Left)).setText(String.valueOf(bundle.getInt("venueTier3") - tier3));
                                ((TextView)findViewById(R.id.tier3Left)).setTextColor(Color.RED);
                            }
                            ((TextView)findViewById(R.id.tier1Left)).setText(String.valueOf(bundle.getInt("venueTier1") - tier1));
                            ((TextView)findViewById(R.id.tier2Left)).setText(String.valueOf(bundle.getInt("venueTier2") - tier2));
                            ((TextView)findViewById(R.id.tier3Left)).setText(String.valueOf(bundle.getInt("venueTier3") - tier3));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        int childCount = coreLayout.getChildCount();

        for (int i = 1; i < childCount; i++) {
            TableRow row = (TableRow) coreLayout.getChildAt(i);

            row.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean flag) {
                    TableRow tablerow = (TableRow) view;
                    ticketType = ((TextView) tablerow.getChildAt(0)).getText().toString();
                    ticketPrice = Float.valueOf(((TextView) tablerow.getChildAt(1)).getText().toString().replaceAll("[\\*£]", ""));
                    TextView price = (TextView) tablerow.getChildAt(1);
                    GridLayout totals = (GridLayout) findViewById(R.id.totalLayout);
                    totals.setVisibility(View.VISIBLE);
                    TextView subTotal = (TextView) findViewById(R.id.subTotal);
                    TextView bookingFee = (TextView) findViewById(R.id.bookingFee);
                    TextView total = (TextView) findViewById(R.id.total);
                    picker.setValue(1);
                    quantity = 1;
                    currentPrice = Float.valueOf(price.getText().toString().substring(1, price.length() - 1));

                    subtotalResult = currentPrice * quantity;
                    float bookingFeeResult = subtotalResult / 10;
                    totalResult = (subtotalResult + bookingFeeResult) * quantity;
                    subTotal.setText(currencyFormatter.format(subtotalResult));
                    bookingFee.setText(currencyFormatter.format(bookingFeeResult));
                    total.setText(currencyFormatter.format(totalResult));
                }
            });
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public void startCheckoutActivity(View view) {
        Intent checkoutIntent = new Intent(this, CheckoutActivity.class);
        checkoutIntent.putExtra("ticketType", ticketType);
        checkoutIntent.putExtra("ticketPrice", ticketPrice);
        checkoutIntent.putExtra("eventName", this.getTitle());
        startActivityForResult(checkoutIntent, 1);
    }

    public void addToBasket(View view) {
        Bundle extras = getIntent().getExtras();
        Artist currentArtist = new Artist(extras.getInt("artistId"), extras.getString("artistName"), extras.getString("artistDesc"));
        Venue currentVenue = new Venue(extras.getInt("venueId"), extras.getString("venueName"), extras.getString("venueDescription"), new Tuple<>(extras.getInt("venueTier1"), extras.getInt("venueTier2"), extras.getInt("venueTier3")), extras.getInt("eventId"));
        BasketSingleton.getMap().put(new Ticket(currentArtist, null, ticketPrice, currentVenue, extras.getString("tourName"), ticketType, extras.getInt("eventId")), picker.getValue());
    }
}
