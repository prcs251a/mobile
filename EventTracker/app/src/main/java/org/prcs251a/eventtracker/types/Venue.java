package org.prcs251a.eventtracker.types;

public class Venue {
    private Integer id;
    private String name;
    private String description;
    private Tuple<Integer, Integer, Integer> ticketLimits;
    private Integer eventId;

    public Venue(Integer id, String name, String description, Tuple<Integer, Integer, Integer> ticketLimits, Integer eventId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.ticketLimits = ticketLimits;
        this.eventId = eventId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Tuple<Integer, Integer, Integer> getTicketLimits() {
        return ticketLimits;
    }

    public Integer getId() {
        return id;
    }

    public Integer getEventId() {
        return eventId;
    }
}
