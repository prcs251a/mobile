package org.prcs251a.eventtracker;

import org.prcs251a.eventtracker.types.Ticket;

import java.util.HashMap;

public class BasketSingleton {
    private static BasketSingleton instance;
    private static HashMap<Ticket, Integer> basketMap;

    private BasketSingleton() {
        basketMap = new HashMap<>();
    }

    public static synchronized BasketSingleton getInstance() {
        if (instance == null) {
            instance = new BasketSingleton();
        }

        return instance;
    }

    public static synchronized HashMap<Ticket, Integer> getMap() {
        if (basketMap == null) {
            basketMap = new HashMap<>();
        }

        return basketMap;
    }

    public void addItem(Ticket ticket) {
        if (basketMap.containsKey(ticket)) {
            basketMap.put(ticket, basketMap.get(ticket) + 1);
        } else {
            basketMap.put(ticket, 1);
        }
    }

    public boolean removeItem(Ticket ticket) {
        if (basketMap.containsKey(ticket)) {
            if (basketMap.get(ticket) == 1) {
                basketMap.remove(ticket);
            } else {
                basketMap.put(ticket, basketMap.get(ticket) - 1);
            }
            return true;
        } else {
            return false;
        }
    }
}
