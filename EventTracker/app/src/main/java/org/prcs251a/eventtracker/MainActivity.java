package org.prcs251a.eventtracker;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.prcs251a.eventtracker.api.Provider;
import org.prcs251a.eventtracker.dialogs.ExceptionDialog;
import org.prcs251a.eventtracker.types.BasketBarActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BasketBarActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static boolean isOnline;
    private SharedPreferences prefs;
    private List<String> listValues;
    private List<JsonObject> values;
    private ArrayAdapter<String> resultsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isOnline = Util.isOnline(getApplicationContext());
        if (!isOnline) {
            ExceptionDialog ex = new ExceptionDialog("Can't detect a network connection, most services will be unavailable.");
            ex.showAlert(MainActivity.this, "Network settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_SETTINGS);
                    startActivity(intent);
                }
            });
        }

        prefs = getSharedPreferences("config", MODE_PRIVATE);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        values = new ArrayList<>();
        listValues = new ArrayList<>();

        values.clear();
        listValues.clear();
        resultsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, listValues);
        ((ListView) findViewById(R.id.searchResultList)).setAdapter(resultsAdapter);
        ((ListView) findViewById(R.id.searchResultList)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent details = new Intent(MainActivity.this, DetailsActivity.class);
                details.putExtra("index", Math.round(Float.valueOf(values.get(position).get("ARTISTID").getAsString())));
                details.putExtra("artist", values.get(position).get("ARTISTNAME").getAsString());
                details.putExtra("desc", values.get(position).get("ARTISTDESCRIPTION").getAsString());
                startActivity(details);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        SearchView searchView = (SearchView) findViewById(R.id.mainSearchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                resultsAdapter.clear();
                values.clear();
                listValues.clear();
                if (isOnline) {
                    if (!newText.isEmpty()) {
                        String url = Provider.baseUrl + "artists/search?q=" + Uri.encode(newText);
                        Log.d("onQueryTextSubmit: ", url);
                        Future<JsonObject> future = Ion.with(MainActivity.this)
                            .load(url)
                            .asJsonObject()
                            .setCallback(new FutureCallback<JsonObject>() {
                                @Override
                                public void onCompleted(Exception e, JsonObject result) {
                                    Ion.getDefault(MainActivity.this).cancelAll(MainActivity.this);
                                    resultsAdapter.clear();
                                    values.clear();
                                    listValues.clear();

                                    try {
                                        if (e != null) {
                                            throw e;
                                        }
                                        Log.d("onCompleted: ", result.toString());
                                        JsonArray results = result.get("name").getAsJsonObject().getAsJsonArray("$values");
                                        for (int i = 0; i < results.size(); i++) {
                                            values.add(results.get(i).getAsJsonObject());
                                            listValues.add(String.valueOf(results.get(i).getAsJsonObject().get("ARTISTNAME")));
                                            resultsAdapter.notifyDataSetChanged();
                                        }
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                }
                            });
                    }
                } else {
                    ExceptionDialog ex = new ExceptionDialog("No network access");
                    ex.showAlert(MainActivity.this);
                }
                return false;
            }
        });


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_login) {
            if (item.getTitle().equals("Login/Register")) {
                Intent loginIntent = new Intent(this, LoginRegisterActivity.class);
                startActivityForResult(loginIntent, 1);
            } else {
                logout();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void updateUserName() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        TextView loggedInName = (TextView) header.findViewById(R.id.loggedInName);
        MenuItem loginText = navigationView.getMenu().getItem(0);
        String userName = prefs.getString("userName", null);
        if (userName != null) {
            loggedInName.setText(userName);
            loginText.setTitle("Logout");
        } else {
            loggedInName.setText(R.string.defaultLoginName);
            loginText.setTitle("Login/Register");
        }
    }

    public void logout() {
        SharedPreferences.Editor resetPrefs = getSharedPreferences("config", MODE_PRIVATE).edit();
        resetPrefs.clear();
        resetPrefs.apply();
        updateUserName();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (prefs.getString("userName", null) != null) {
            Toast.makeText(MainActivity.this, "Welcome, " + prefs.getString("userName", null), Toast.LENGTH_LONG).show();
        }
        updateUserName();
    }

    public void onEventClick(View view) {
        Intent detailsIntent = new Intent(this, DetailsActivity.class);
        startActivityForResult(detailsIntent, 1);
    }
}