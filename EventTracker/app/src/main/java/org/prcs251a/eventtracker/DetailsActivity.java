package org.prcs251a.eventtracker;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.prcs251a.eventtracker.api.Provider;
import org.prcs251a.eventtracker.types.Artist;
import org.prcs251a.eventtracker.types.BasketBarActivity;
import org.prcs251a.eventtracker.types.Tuple;
import org.prcs251a.eventtracker.types.Venue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DetailsActivity extends BasketBarActivity {
    private ExpandableAdapter resultsAdapter;
    private View progressView;
    private HashMap<String, List<Venue>> itemList;
    private List<Venue> venueMap;
    private Artist artist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressView = findViewById(R.id.detailsProgressBar);
        itemList = new HashMap<>();
        ExpandableListView listView = (ExpandableListView) findViewById(R.id.detailsEventList);
        resultsAdapter = new ExpandableAdapter(DetailsActivity.this, itemList);
        listView.setAdapter(resultsAdapter);
        venueMap = new ArrayList<>();

        showProgress(true);
        String url = Provider.baseUrl + "events/search?q=" + Uri.encode(getIntent().getExtras().getString("artist")) + "&types=artist";
        Log.d("onQueryTextSubmit: ", url);
        Future<JsonObject> future = Ion.with(DetailsActivity.this)
                .load(url)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        showProgress(false);
                        try {
                            Log.d("onCompleted: ", result.toString());
                            JsonArray results = result.get("artist").getAsJsonObject().getAsJsonArray("$values");
                            if (results.size() == 0) {
                                ((TextView) findViewById(R.id.detailsPrompt)).setText("");
                                ((TextView) findViewById(R.id.noEventsString)).setText(R.string.no_artists);
                                return;
                            }
                            artist = new Artist(getIntent().getExtras().getInt("index"), getIntent().getExtras().getString("artist"), getIntent().getExtras().getString("desc"));
                            resultsAdapter.setCurrentArtist(artist);
                            setTitle(artist.getName());
                            ((TextView) findViewById(R.id.detailsDescription)).setText(artist.getDescription());
                            for (int i = 0; i < results.size(); i++) {
                                JsonObject currentTour = results.get(i).getAsJsonObject().get("TOUR").getAsJsonObject();
                                JsonObject currentVenue = results.get(i).getAsJsonObject().get("VENUE").getAsJsonObject();
                                Venue venue = new Venue(currentVenue.get("VENUEID").getAsInt(), currentVenue.get("VENUENAME").getAsString(), currentVenue.get("VENUEDESCRIPTION").getAsString(), new Tuple<>(currentVenue.get("STANDINGCAPACITY").getAsInt(), currentVenue.get("FIRSTTIERCAPACITY").getAsInt(), currentVenue.get("SECONDTIERCAPACITY").getAsInt()), results.get(i).getAsJsonObject().get("EVENTID").getAsInt());
                                venueMap.add(venue);
                                itemList.put(currentTour.get("TOURNAME").getAsString(), venueMap);
                            }
                            resultsAdapter.notifyDataSetChanged();
                        } catch (Exception ex) {
                            ((TextView) findViewById(R.id.detailsPrompt)).setText("");
                            ex.printStackTrace();
                        }
                    }
                });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }
}
