package org.prcs251a.eventtracker.types;

public class Artist {
    private Integer id;
    private String name;
    private String description;

    public Artist(Integer id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Integer getId() {
        return id;
    }
}
