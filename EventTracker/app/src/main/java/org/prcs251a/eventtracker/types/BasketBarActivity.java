package org.prcs251a.eventtracker.types;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.prcs251a.eventtracker.BasketActivity;
import org.prcs251a.eventtracker.BasketSingleton;
import org.prcs251a.eventtracker.R;

public class BasketBarActivity extends AppCompatActivity {
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_basket:
                Intent basketIntent = new Intent(this, BasketActivity.class);
                startActivityForResult(basketIntent, 1);
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (BasketSingleton.getMap().size() == 0) {
            inflater.inflate(R.menu.main, menu);
        } else {
            inflater.inflate(R.menu.main_full, menu);
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        this.supportInvalidateOptionsMenu();
        menu.findItem(R.id.action_basket).setTitle(String.valueOf(BasketSingleton.getMap().size()));
        return true;
    }
}
