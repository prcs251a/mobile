package org.prcs251a.eventtracker;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.prcs251a.eventtracker.api.Provider;
import org.prcs251a.eventtracker.dialogs.ErrorDialog;
import org.prcs251a.eventtracker.types.Customer;

import java.util.List;
import java.util.Locale;

import static org.prcs251a.eventtracker.Util.Password.getSaltedHash;

/**
 * A login screen that offers login via email/password.
 */
public class RegisterFragment extends Fragment {

    // UI references.

    private EditText emailView;
    private EditText password1View;
    private EditText password2View;
    private View progressView;
    private View loginFormView;
    private FragmentActivity fragmentActivity;
    private EditText firstNameView;
    private EditText lastNameView;
    private EditText cityView;
    private EditText addressLine1View;
    private EditText addressLine2View;
    private EditText postCodeView;
    private ScrollView scrollView;
    private EditText usernameView;
    private Spinner countySpinner;

    public RegisterFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentActivity = super.getActivity();
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.fragment_register, container, false);

        Button mEmailSignInButton = (Button) layout.findViewById(R.id.email_register_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegister();
            }
        });

        countySpinner = (Spinner) layout.findViewById(R.id.registerCountySpinner);

        emailView = (EditText) layout.findViewById(R.id.registerEmail);

        password1View = (EditText) layout.findViewById(R.id.password_register_1);
        password2View = (EditText) layout.findViewById(R.id.password_register_2);

        loginFormView = layout.findViewById(R.id.registerLayout);
        progressView = layout.findViewById(R.id.registerProgress);
        firstNameView = (EditText) layout.findViewById(R.id.registerFirstName);
        lastNameView = (EditText) layout.findViewById(R.id.registerLastName);
        cityView = (EditText) layout.findViewById(R.id.registerCity);
        addressLine1View = (EditText) layout.findViewById(R.id.registerAddressLine1);
        addressLine2View = (EditText) layout.findViewById(R.id.registerAddressLine2);
        postCodeView = (EditText) layout.findViewById(R.id.registerPostcode);
        usernameView = (EditText) layout.findViewById(R.id.registerUsername);

        scrollView = (ScrollView) layout.findViewById(R.id.registerForm);
        return layout;
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptRegister() {
        if (!Util.isOnline(getContext())) {
            return;
        }

        // Reset errors.
        firstNameView.setError(null);
        lastNameView.setError(null);
        cityView.setError(null);
        addressLine1View.setError(null);
        addressLine2View.setError(null);
        postCodeView.setError(null);
        emailView.setError(null);
        password1View.setError(null);
        password2View.setError(null);
        usernameView.setError(null);

        boolean cancel = false;
        View focusView = null;

        // Check if passwords are the same
        if (!arePasswordsSame()) {
            password2View.setError(getString(R.string.error_different_passwords));
            focusView = password2View;
            cancel = true;
        }

        // Store values at the time of the login attempt.
        String firstName = firstNameView.getText().toString();
        String lastName = lastNameView.getText().toString();
        String city = cityView.getText().toString();
        String addressLine1 = addressLine1View.getText().toString();
        String addressLine2 = addressLine2View.getText().toString();
        final String email = emailView.getText().toString();
        String password1 = password1View.getText().toString();
        String password2 = password2View.getText().toString();
        String postcode = postCodeView.getText().toString();
        final String username = usernameView.getText().toString();

        if (TextUtils.isEmpty(postcode)) {
            postCodeView.setError(getString(R.string.error_field_required));
            focusView = postCodeView;
            cancel = true;
        }

        if (TextUtils.isEmpty(city)) {
            cityView.setError(getString(R.string.error_field_required));
            focusView = cityView;
            cancel = true;
        }

        if (TextUtils.isEmpty(addressLine2)) {
            addressLine2View.setError(getString(R.string.error_field_required));
            focusView = addressLine2View;
            cancel = true;
        }

        if (TextUtils.isEmpty(addressLine1)) {
            addressLine1View.setError(getString(R.string.error_field_required));
            focusView = addressLine1View;
            cancel = true;
        }

        if (!TextUtils.isEmpty(password2) && !isPasswordValid(password2)) {
            password2View.setError(getString(R.string.error_invalid_password));
            focusView = password2View;
            cancel = true;
        }

        if (!TextUtils.isEmpty(password1) && !isPasswordValid(password1)) {
            password1View.setError(getString(R.string.error_invalid_password));
            focusView = password1View;
            cancel = true;
        }

        if (TextUtils.isEmpty(username)) {
            usernameView.setError(getString(R.string.error_field_required));
            focusView = usernameView;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            emailView.setError(getString(R.string.error_field_required));
            focusView = emailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            emailView.setError(getString(R.string.error_invalid_email));
            focusView = emailView;
            cancel = true;
        }

        if (TextUtils.isEmpty(lastName)) {
            lastNameView.setError(getString(R.string.error_field_required));
            focusView = lastNameView;
            cancel = true;
        }

        if (TextUtils.isEmpty(firstName)) {
            firstNameView.setError(getString(R.string.error_field_required));
            focusView = firstNameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            scrollView.scrollTo(0, (int) focusView.getY() - 20);

        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            String password = null;
            try {
                password = getSaltedHash(password1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            showProgress(true);
            String url = Provider.baseUrl + String.format(Locale.getDefault(), "auth/register?name=%s&username=%s&email=%s&password=%s&postcode=%s&county=%s&city=%s&line1=%s&line2=%s",
                    Uri.encode(String.format("%s %s", firstName, lastName)),
                    Uri.encode(username),
                    Uri.encode(email),
                    Uri.encode(password),
                    Uri.encode(postcode),
                    (countySpinner.getSelectedItemPosition() + 1),
                    Uri.encode(city),
                    Uri.encode(addressLine1),
                    Uri.encode(addressLine2));
            Log.d("onQueryTextSubmit: ", url);
            Future<Response<String>> future = Ion.with(getContext())
                .load("POST", url)
                .setBodyParameter("foo", "bar")
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        showProgress(false);
                        if (result.getHeaders().code() == 200) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle("Register successful.")
                                    .setMessage("Confirmation sent to " + email + ". Please verify within 24 hours.")
                                    .setNeutralButton("Ok", null);
                            builder.create().show();
                            fragmentActivity.finish();
                        } else {
                            new ErrorDialog(getContext(), String.format("Something went wrong (%s)", result.getHeaders().code()));
                        }
                    }
                });
        }
    }

    private boolean isEmailValid(String email) {
        return email.matches("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$");
    }

    private boolean isPasswordValid(String password) {
        return password.matches("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*!|@|%|#).{8,20}$");
    }

    private boolean arePasswordsSame() { return password1View.getText().toString().equals(password2View.getText().toString()); }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            loginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

