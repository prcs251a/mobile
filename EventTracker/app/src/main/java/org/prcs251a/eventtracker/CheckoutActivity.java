package org.prcs251a.eventtracker;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.prcs251a.eventtracker.api.Provider;
import org.prcs251a.eventtracker.types.Pair;
import org.prcs251a.eventtracker.types.Ticket;

import java.io.LineNumberReader;
import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class CheckoutActivity extends AppCompatActivity {
    private Bundle extras;
    private float total;
    private String email;
    private List<Pair<Integer, Integer>> purchaseMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        extras = getIntent().getExtras();
        purchaseMap = new ArrayList<>();
        List<String> paymentMethods = new ArrayList<>();

        paymentMethods.add("Example PayPal");
        paymentMethods.add("Example Credit Card");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        LinearLayout rootLayout = (LinearLayout) findViewById(R.id.checkoutLayout);

        for (HashMap.Entry<Ticket, Integer> entry : BasketSingleton.getMap().entrySet()) {
            final Ticket t = entry.getKey();
            purchaseMap.add(new Pair<>(t.getVenue().getEventId(), Integer.valueOf(t.getType().substring(t.getType().length() - 1))));
            total += ((t.getPrice() + (t.getPrice() / 10)) * entry.getValue());
            GradientDrawable border = new GradientDrawable();
            border.setColor(0xFFFFFFFF);
            border.setStroke(1, 0xFF000000);
            RelativeLayout layout = new RelativeLayout(CheckoutActivity.this);
            TextView quantity = new TextView(CheckoutActivity.this);
            TextView eventName = new TextView(CheckoutActivity.this);
            TextView ticketType = new TextView(CheckoutActivity.this);
            TextView venueName = new TextView(CheckoutActivity.this);

            quantity.setText(String.format("%sx ",entry.getValue()));
            quantity.setTypeface(null, Typeface.BOLD);
            quantity.setTextSize(24);
            eventName.setTypeface(null, Typeface.BOLD);
            eventName.setText(t.getTour());
            venueName.setText(t.getVenue().getName());
            ticketType.setText(t.getType());

            quantity.setId(View.generateViewId());
            eventName.setId(View.generateViewId());
            venueName.setId(View.generateViewId());
            ticketType.setId(View.generateViewId());

            RelativeLayout.LayoutParams quantityParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            RelativeLayout.LayoutParams venueParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            RelativeLayout.LayoutParams eventParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            RelativeLayout.LayoutParams typeParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            quantityParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            quantityParams.addRule(RelativeLayout.LEFT_OF, ticketType.getId());
            venueParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            venueParams.addRule(RelativeLayout.BELOW, ticketType.getId());
            eventParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            typeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            typeParams.addRule(RelativeLayout.BELOW, eventName.getId());

            quantityParams.topMargin = 25;
            venueParams.leftMargin = venueParams.rightMargin = venueParams.topMargin = 5;
            venueParams.bottomMargin = 10;
            eventParams.leftMargin = eventParams.rightMargin = eventParams.bottomMargin = eventParams.topMargin = 5;
            typeParams.leftMargin = typeParams.rightMargin = 5;
            layout.addView(quantity, quantityParams);
            layout.addView(eventName, eventParams);
            layout.addView(venueName, venueParams);
            layout.addView(ticketType, typeParams);


            rootLayout.addView(layout);
        }

        ((Button)findViewById(R.id.payButton)).setText(String.format("%s %s", ((Button) findViewById(R.id.payButton)).getText(), String.valueOf(NumberFormat.getCurrencyInstance().format(total))));

        ArrayAdapter<String> paymentAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, paymentMethods);
        paymentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner paymentItems = (Spinner) findViewById(R.id.paymentSpinner);
        paymentItems.setAdapter(paymentAdapter);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public void startConfirmActivity(View view) {
        final ProgressDialog progressDialog;
        progressDialog = ProgressDialog.show(CheckoutActivity.this, "Working", "This is where you would normally be redirected to PayPal/bank auth.", true);
        SharedPreferences prefs = getSharedPreferences("config", MODE_PRIVATE);
        Integer userId = prefs.getInt("userId", 0);
        Ion.with(getApplicationContext())
                .load(Provider.baseUrl + "users/" + userId)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        Gson gson = new GsonBuilder().serializeNulls().create();
                        HashMap<String, Number> data = new HashMap<>();
                        final List<Pair<Integer, Integer>> tickets = new ArrayList<>();
                        email = result.getAsJsonObject().get("EMAIL").getAsString();
                        data.put("CustomerID", result.getAsJsonObject().get("CUSTOMERID").getAsInt());
                        data.put("Cost", total);
                        for (Pair<Integer, Integer> entry : purchaseMap) {
                            tickets.add(new Pair<>(entry.first(), entry.second()));
                        }
                        JsonParser parser = new JsonParser();
                        JsonElement dataJson = parser.parse(gson.toJson(data));
                        JsonElement ticketJson = parser.parse(gson.toJson(tickets).replaceAll("first", "EventID").replaceAll("second", "TierID"));
                        JsonObject dataObject = new JsonObject();
                        dataObject.add("data", dataJson);
                        dataObject.add("tickets", ticketJson);

                        Ion.with(getApplicationContext())
                                .load("POST", Provider.baseUrl + "tickets/purchaseTickets")
                                .setBodyParameter("json", String.valueOf(dataObject))
                                .asString()
                                .setCallback(new FutureCallback<String>() {
                                    @Override
                                    public void onCompleted(Exception e, String result) {
                                        progressDialog.dismiss();
                                        Intent confirmIntent = new Intent(CheckoutActivity.this, ConfirmActivity.class);
                                        confirmIntent.putExtra("cost", total);
                                        confirmIntent.putExtra("count", tickets.size());
                                        confirmIntent.putExtra("email", email);
                                        confirmIntent.putExtra("bookingId", Float.valueOf(result).intValue());
                                        startActivityForResult(confirmIntent, 1);
                                    }
                                });
                    }
                });
    }

}
